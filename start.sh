#!/bin/bash

chown -R application:www-data /var/www/app

# Run composer
/usr/bin/composer install

# Set up .env file
if [ ! -f "./.env" ]; then
    cp .env.example .env ;
fi

# Do some laravel stuff
php artisan key:generate
php artisan config:cache
php artisan migrate --seed
